# PHP to HTML Boilerplate

### Installation

Requires [Node.js](https://nodejs.org/)  and gulp-cli to run.

Install gulp-cli if you haven't yet

```sh
$ npm install -g gulp-cli
```

Install the dependencies and devDependencies.

```sh
$ cd php-to-html-boilerplate
$ npm install
```

Run gulp

```sh
$ gulp
```

Create HTML files from PHP

```sh
$ gulp build-html
```