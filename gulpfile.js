var gulp         = require('gulp');
var php2html     = require('gulp-php2html');
var htmlbeautify = require('gulp-html-beautify');
var clean        = require('gulp-clean');
var sass         = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var plumber      = require('gulp-plumber');
var concat       = require('gulp-concat');
var minify       = require('gulp-minify');
var php          = require('gulp-connect-php');


gulp.task('generate-html', function() {
    return gulp.src('./src/*.php')
        .pipe(plumber())
        .pipe(php2html())
        .pipe(htmlbeautify())
        .pipe(gulp.dest('./html'));
});
gulp.task('delete-html', function() {
    return gulp.src('./html/*.html')
        .pipe(plumber())
        .pipe(clean({
            force: true
        }));
});

gulp.task('build-html', gulp.series('delete-html', 'generate-html'));

gulp.task('styles', function() {

    var config = {
        outputStyle: 'compressed'
    };
    return gulp.src('./src/assets/scss/**/*.scss')
        .pipe(plumber())
        .pipe(sass(config).on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: ['last 100 versions'],
            cascade: false
        }))
        .pipe(gulp.dest('./src/assets/dist'));
});

gulp.task('scripts', function () {
    return gulp.src('./src/assets/js/*.js')
        .pipe(plumber())
        .pipe(concat('main.js'))
        .pipe(minify())
        .pipe(gulp.dest('./src/assets/dist'));
});

gulp.task('serve', function() {
    php.server({ base: './src', port: 8000, keepalive: true });
});

gulp.task('watch', function() {
    gulp.watch('./src/assets/scss/**/*.scss', gulp.series('styles'));
    gulp.watch('./src/assets/js/**/*.js', gulp.series('scripts'));
});

gulp.task('default', gulp.series(
    'styles',
    'scripts',
    gulp.parallel('watch', 'serve')
));